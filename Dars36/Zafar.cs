﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dars36
{
    public class Zafar
    {
        public int Age { get; set; }
        public string Name { get; set; }
        public Zafar(int age, string name)
        {
            Age = age;
            Name = name;
        }   
    }
}
