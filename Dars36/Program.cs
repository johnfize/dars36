﻿var name = Console.ReadLine();

if (string.IsNullOrWhiteSpace(name))
{
    Console.WriteLine("You should enter your name");
}
else
{
    Console.WriteLine($"Hello, {name}!");
}
